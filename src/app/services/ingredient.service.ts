/**
 * Created by tbriggs on 2/11/2017.
 */

import { Injectable } from "@angular/core";
import { Ingredient } from "../interfaces/ingredient.interface";

@Injectable()
export class IngredientService {
    getIngredientsByCategory(category: string): Ingredient[] {
        switch (category) {
            case 'Vegetable':
                return [
                    {
                        id: 3,
                        name: 'Artichokes',
                        category: 'Vegetable',
                        metric: 'Ounces (oz)',
                        amount: 8
                    },
                    {
                        id: 20,
                        name: 'Carrots',
                        category: 'Vegetable',
                        metric: 'Ounces (oz)',
                        amount: 8
                    }
                ];
            case 'Fruit':
                return [
                    {
                        id: 1,
                        name: 'Apple',
                        category: 'Fruit',
                        metric: 'Whole',
                        amount: 1
                    },
                    {
                        id: 2,
                        name: 'Apricot',
                        category: 'Fruit',
                        metric: 'Whole',
                        amount: 1
                    }
                ];
            case 'Protein':
                return [
                    {
                        id: 86,
                        name: 'Yogurt',
                        category: 'Protein',
                        metric: 'Ounces (oz)',
                        amount: 8
                    }
                ];
            case 'Grain':
                return [
                    {
                        id: 28,
                        name: 'Corn',
                        category: 'Grain',
                        metric: 'Ounces (oz)',
                        amount: 4
                    }
                ];
            case 'Fat':
                return [
                    {
                        id: 5,
                        name: 'Avocado',
                        category: 'Fat',
                        metric: 'Whole',
                        amount: 0.125
                    }
                ];
            case 'Dressing':
                return [
                    {
                        id: 70,
                        name: 'Ranch',
                        category: 'Dressing',
                        metric: 'Table Spoons (Tbsp)',
                        amount: 2
                    }
                ];
            case 'Condiment':
                return [
                    {
                        id: 45,
                        name: 'Ketchup',
                        category: 'Condiment',
                        metric: 'Cups (c)',
                        amount: 0.5
                    }
                ];
        }
    }

    saveIngredient(name: string, category: string, metric: string, amount: number): void {

    }
}