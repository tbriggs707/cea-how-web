/**
 * Created by tbriggs on 2/11/2017.
 */

class Fruit {
    public name: string = 'Fruit';
    public isExpanded: boolean = false;
}

class Vegetable {
    public name: string = 'Vegetable';
    public isExpanded: boolean = false;
}

class Fat {
    public name: string = 'Fat';
    public isExpanded: boolean = false;
}

class Grain {
    public name: string = 'Grain';
    public isExpanded: boolean = false;
}

class Condiment {
    public name: string = 'Condiment';
    public isExpanded: boolean = false;
}

class Dressing {
    public name: string = 'Dressing';
    public isExpanded: boolean = false;
}

class Protein {
    public name: string = 'Protein';
    public isExpanded: boolean = false;
}

export interface Categories {
    fruit: Fruit;
    vegetable: Vegetable;
    fat: Fat;
    grain: Grain;
    condiment: Condiment;
    dressing: Dressing;
    protein: Protein;
}