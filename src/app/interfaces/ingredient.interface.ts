/**
 * Created by tbriggs on 2/11/2017.
 */
export interface Ingredient {
    id: number;
    name: string;
    category: string;
    metric: string;
    amount: number;
}