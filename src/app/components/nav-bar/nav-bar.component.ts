import {Component, OnInit, Input, AfterViewInit} from '@angular/core';

@Component({
    selector: 'nav-bar',
    templateUrl: 'nav-bar.component.html',
    styleUrls: ['nav-bar.component.css']
})
export class NavBarComponent implements OnInit, AfterViewInit {
    @Input() public pageName: string = 'home';

    constructor() {
    }

    setActive() {
        if (this.pageName === 'recipes') {
            document.getElementById('home').classList.remove('active');
            document.getElementById('recipes').classList.add('active');
            document.getElementById('ingredients').classList.remove('active');
        } else if (this.pageName === 'ingredients') {
            document.getElementById('home').classList.remove('active');
            document.getElementById('recipes').classList.remove('active');
            document.getElementById('ingredients').classList.add('active');
        } else { // home
            document.getElementById('home').classList.add('active');
            document.getElementById('recipes').classList.remove('active');
            document.getElementById('ingredients').classList.remove('active');
        }
    }

    ngOnInit() {
    }

    ngAfterViewInit() {
        this.setActive();
    }

}
