import {Component, OnInit, Input, ViewChild} from '@angular/core';
import {ModalDirective} from 'ng2-bootstrap/modal';
import {NgForm} from '@angular/forms';
import {IngredientService} from "../../services/ingredient.service";

@Component({
    selector: 'add-edit-ingredient-modal',
    templateUrl: './add-edit-ingredient-modal.component.html',
    styleUrls: ['./add-edit-ingredient-modal.component.css']
})
export class AddEditIngredientModalComponent implements OnInit {
    @Input() public modalTitle: string;
    public categories: string[] = [
        'Vegetable',
        'Fruit',
        'Protein',
        'Fat',
        'Grain',
        'Condiment',
        'Dressing'
    ];
    public name: string;
    public category: string = '';
    public metric: string = '';
    public amount: number;
    @ViewChild('ingredientModal') public ingredientModal: ModalDirective;
    @ViewChild('form') public form: NgForm;

    public showModal(type: string): void {
        this.modalTitle = type === 'add' ? 'Add Ingredient' : 'Edit Ingredient';
        this.ingredientModal.show();
    }

    public hideModal(): void {
        this.ingredientModal.hide();
    }

    validateName(): boolean {
        return (!!this.name && !!this.name.trim());
    }

    categoryChange(newValue: string): void {
        this.category = newValue;
    }

    metricChange(newValue: string): void {
        this.metric = newValue;
    }

    amountChange(newValue: number): void {
        this.amount = newValue;
    }

    saveIngredient(): void {
        console.log('Name: ' + this.name);
        console.log('Category: ' + this.category);
        console.log('Metric: ' + this.metric);
        console.log('Amount: ' + this.amount);
        this.ingredientService.saveIngredient(this.name, this.category, this.metric, this.amount);
        this.hideModal();
    }

    clearForm() {
        // this.form.resetForm();
    }

    constructor(private ingredientService: IngredientService) {
    }

    ngOnInit() {
    }

}
